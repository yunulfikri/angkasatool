const express = require('express');
var fs = require('fs');
var cors = require('cors')
var http = require('http');
var https = require('https');
const ExcelJS = require('exceljs');
const { Sequelize , QueryTypes} = require('sequelize');
var path = require('path');

var privateKey  = fs.readFileSync(__dirname + '/selfsigned.key', 'utf8');
var certificate = fs.readFileSync(__dirname + '/selfsigned.crt', 'utf8');

var credentials = {key: privateKey, cert: certificate};


var app = express()
const port = 8081
app.use(cors());
// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Methods", "GET, PUT, POST");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });
var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

httpServer.listen(8080, () => {
  console.log("serever is runing at port 8080");
});
httpsServer.listen(8081, () => {
  console.log("serever is runing at port 8081");
});

var FILES_DIR = path.join(__dirname, 'files')
// var PUBLIC_DIR = path.join(__dirname, 'public')


app.get('/', (req, res) => {

  res.send('Fuck EXcel')
})
app.get('/generate', async (req, res)=> {
  const idreq=req.query.id
  await generate(idreq, res)
})


app.get('/download', async function(req, res, next){
  const sequelize = new Sequelize('cyber_angkasa_tool', 'cyber_admin', 'Gabyjkt123', {
    host: 'localhost',
    dialect: 'mysql'
});
  const idreq=req.query.id
  const dataExcel = await sequelize.query("SELECT name FROM `excels` where id=" + idreq + " LIMIT 1", { type: QueryTypes.SELECT });
  var excelNameFile = dataExcel[0]['name']+".xlsx";
    res.download(excelNameFile, { root: FILES_DIR }, function (err) {
      if (!err) return; // file sent
      if (err.status !== 404) return next(err); // non-404 error
      // file for download not found
      res.statusCode = 404;
      res.send('Cant find that file, sorry! please generate again');
    });
});


app.get('*', (req, res) => {
    res.send('Salah URL woy, balik kanan')
  })

  
// app.listen(port, () => {
//   console.log(`Example app listening on port ${port}`)
// })


async function generate(excel_id, res){
  const workbook = new ExcelJS.Workbook()
  const workbookMaster = await workbook.xlsx.readFile(__dirname + '/Master.xlsx')
  const sequelize = new Sequelize('cyber_angkasa_tool', 'cyber_admin', 'Gabyjkt123', {
      host: '127.0.0.1',
      dialect: 'mysql'
  });

//   const sequelize = new Sequelize('angkasatool', 'root', '', {
//     host: '127.0.0.1',
//     dialect: 'mysql'
// });

  const dataExcel = await sequelize.query("SELECT name FROM `excels` where id=" + excel_id + " LIMIT 1", { type: QueryTypes.SELECT });
  var excelName = dataExcel[0]['name'];

  // YOUTUBE BAGIAN
  const worksheetYoutube = workbookMaster.getWorksheet('YOUTUBE')
  const dataYoutube = await sequelize.query("SELECT account,link FROM `link_excels` where sosialmedia='Youtube' and excel_id=" + excel_id, { type: QueryTypes.SELECT });
  var accountYoutube = "";   
  dataYoutube.forEach(function (item,i){
      var row = 4+i;
      worksheetYoutube.getCell('D'+row).value = item['link'];
      if (accountYoutube != item['account']) {
          worksheetYoutube.getCell('C'+row).value = item['account'];
      }
      accountYoutube = item['account']
  });


  // FACEBOOK BAGIAN
  const worksheetFacebook = workbookMaster.getWorksheet('FACEBOOK')
  const dataFacebook = await sequelize.query("SELECT account,link FROM `link_excels` where sosialmedia='Facebook' and excel_id=" + excel_id, { type: QueryTypes.SELECT });
  var accountFacebook = "";   
  dataFacebook.forEach(function (item,i){
      var row = 4+i;
      worksheetFacebook.getCell('D'+row).value = item['link'];
      if (accountFacebook != item['account']) {
          worksheetFacebook.getCell('C'+row).value = item['account'];
      }
      accountFacebook = item['account']
  });

   // INSTAGRAM BAGIAN
   const worksheetInstagram = workbookMaster.getWorksheet('INSTAGRAM')
   const dataInstagram = await sequelize.query("SELECT account,link FROM `link_excels` where sosialmedia='Instagram' and excel_id=" + excel_id, { type: QueryTypes.SELECT });
   var accountInstagram = "";   
   dataInstagram.forEach(function (item,i){
       var row = 4+i;
       worksheetInstagram.getCell('D'+row).value = item['link'];
       if (accountInstagram != item['account']) {
           worksheetInstagram.getCell('C'+row).value = item['account'];
       }
       accountInstagram = item['account']
   });

  // TWITTER BAGIAN
  const worksheetTwitter = workbookMaster.getWorksheet('TWITTER')
  const dataTwitter = await sequelize.query("SELECT account,link FROM `link_excels` where sosialmedia='Twitter' and excel_id=" + excel_id, { type: QueryTypes.SELECT });
  var accountTwitter = "";   
  dataTwitter.forEach(function (item,i){
      var row = 4+i;
      worksheetTwitter.getCell('D'+row).value = item['link'];
      if (accountTwitter != item['account']) {
          worksheetTwitter.getCell('C'+row).value = item['account'];
      }
      accountTwitter = item['account']
  });

  // TIKTOK BAGIAN
  const worksheetTiktok = workbookMaster.getWorksheet('TIKTOK')
  const dataTiktok = await sequelize.query("SELECT account,link FROM `link_excels` where sosialmedia='Tiktok' and excel_id=" + excel_id, { type: QueryTypes.SELECT });
  var accountTiktok = "";   
  dataTiktok.forEach(function (item,i){
      var row = 4+i;
      worksheetTiktok.getCell('D'+row).value = item['link'];
      if (accountTiktok != item['account']) {
          worksheetTiktok.getCell('C'+row).value = item['account'];
      }
      accountTiktok = item['account']
  });

  // SNACKVIDEO BAGIAN
  const worksheetSnackvideo = workbookMaster.getWorksheet('SNACKVIDEO')
  const dataSnackvideo = await sequelize.query("SELECT account,link FROM `link_excels` where sosialmedia='Snackvideo' and excel_id=" + excel_id, { type: QueryTypes.SELECT });
  var accountSnackvideo = "";   
  dataSnackvideo.forEach(function (item,i){
      var row = 4+i;
      worksheetSnackvideo.getCell('D'+row).value = item['link'];
      if (accountSnackvideo != item['account']) {
          worksheetSnackvideo.getCell('C'+row).value = item['account'];
      }
      accountSnackvideo = item['account']
  });

  

  // wRITE TO FIle Fucker
  // await workbookMaster.xlsx.writeFile('files/'+ excelName + '.xlsx');
  await workbookMaster.xlsx.writeFile('../public/download/'+ excelName + '.xlsx');


  // res.send("Wait 30sec!")
  var excelNameFile = dataExcel[0]['name']+".xlsx";
  res.json({ download: 1, filename:excelNameFile });
  // res.download(excelNameFile, { root: FILES_DIR }, function (err) {
  //   if (!err) return; // file sent
  //   if (err.status !== 404) return next(err); // non-404 error
  //   // file for download not found
  //   res.statusCode = 404;
  //   res.send('Cant find that file, sorry! please generate again');
  // });
}