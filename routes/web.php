<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;


use App\Http\Controllers\TaskController;
use App\Http\Controllers\SubTaskController;
use App\Http\Controllers\LinkController;
use App\Http\Controllers\ExcelController;
use App\Http\Controllers\LinkExcelController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {

    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');

    Route::get('/tasks', [TaskController::class, 'index'])->name('task');
    Route::post('/tasks/store', [TaskController::class, 'store'])->name('task.store');
    Route::post('/tasks/destroy/', [TaskController::class, 'destroy'])->name('task.destroy');

    Route::get('/tasks/view/api/{id}', [TaskController::class, 'show'])->name('task.show');



    Route::get('/tasks/{id}', [SubTaskController::class, 'index'])->name('subtask');

    Route::get('/tasks/{id}/getchart', [SubTaskController::class, 'getchart'])->name('subtask.getchart');

    Route::post('/subtasks/store', [SubTaskController::class, 'store'])->name('subtask.store');
    Route::post('/subtasks/update', [SubTaskController::class, 'updateakun'])->name('subtask.updateakun');
    Route::post('/subtasks/delete/', [SubTaskController::class, 'destroy'])->name('subtask.destroy');


    Route::get('/subtask/{id}/links', [LinkController::class, 'index'])->name('links');


    Route::post('/links/store', [LinkController::class, 'store'])->name('links.store');
    Route::post('/links/destroy', [LinkController::class, 'destroy'])->name('links.destroy');

    Route::get('/generate/{id}', [SubTaskController::class, 'generate'])->name('generate');



    // ============== BAGIIAN EXCELLLLLL ===================

    Route::get('/excel', [ExcelController::class, 'index'])->name('excel');
    Route::post('/excel/store', [ExcelController::class, 'store'])->name('excel.store');
    Route::post('/excel/destroy/', [ExcelController::class, 'destroy'])->name('excel.destroy');

    Route::get('/excel/{id}/links', [LinkExcelController::class, 'index'])->name('excellinks');
    Route::post('/excel/links/store', [LinkExcelController::class, 'store'])->name('excellinks.store');
    Route::post('/excel/links/destroy', [LinkExcelController::class, 'destroy'])->name('excellinks.destroy');

    Route::get('/excel/{id}/export', [LinkExcelController::class, 'export'])->name('excellinks.export');
    
    Route::get('/excel/download', [LinkExcelController::class, 'download'])->name('excellinks.download');


});
