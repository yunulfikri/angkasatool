<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Task;

class TaskController extends Controller
{
    //
    public function index()
    {
        # code...
        return Inertia::render('Task', ['taskdata' => Task::latest()->get()]);

    }
    public function store(Request $request)
    {
        # code...
        Task::create([
            'name' => $request->task_input,
        ]);
        return back();
    }
    public function show($id)
    {
        # code...
        //SELECT COUNT(links.link), users.name FROM `links` 
        // INNER JOIN `users` ON users.id = links.user_id INNER JOIN sub_tasks ON sub_tasks.id = links.subtask_id 
        // INNER JOIN tasks ON tasks.id = sub_tasks.task_id 
        // WHERE tasks.id = 1 GROUP BY links.user_id; 

        
        $taskdata = Task::find($id);
        return $taskdata;
    }
    public function destroy(Request $request)
    {
        # code...
        Task::destroy($request->id);
        return 'sukses';
    }
    

}
