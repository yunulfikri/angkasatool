<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Inertia\Inertia;
use App\Models\LinkExcel;
use App\Models\Excel;
use Illuminate\Support\Facades\DB;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx as ReaderXlsx;

class LinkExcelController extends Controller
{
    //
    public function index($id)
    {
        # code...
        $subtask = Excel::find($id);
        $user = Auth::user();

        //select distinct(links.user_id),users.name from links inner join users on users.id = links.user_id where subtask_id = 10;
        // $userVirall = LinkExcel::distinct('link_excels.user_id')->select('users.id','users.name')
        //             ->where('excel_id', $id)
        //             ->join('users','link_excels.user_id','=','users.id')->get();
        $getChart = LinkExcel::select(DB::raw('count(link_excels.link) as user_count, users.name'))->where('excels.id',$id)->where('excels.deleted_at', Null)
        ->join('users', 'users.id', '=', 'link_excels.user_id')
        ->join('excels', 'excels.id', '=', 'link_excels.excel_id')
        ->groupBy('link_excels.user_id')->get();
        $ownLink = LinkExcel::where('user_id',$user->id)
        ->where('excel_id', $id)
        ->orderBy('account')->get();
        // return $ownLink;
        return Inertia::render('ExcelLink', [
            'subtask' => $subtask,
            'datauser' => $user,
            'getchart' => $getChart,
            'ownlink' => $ownLink,
            'links' => LinkExcel::select('link_excels.*','users.name as operator')->where('link_excels.excel_id', $id)->join('users', 'users.id','=', 'link_excels.user_id')->get()
        ]);
    }
    public function store(Request $request){
        
        $user = Auth::user();
        $data = $request->links;
        $partial= explode("\n", $data);
        foreach ($partial as $value) {
            # check linkk kosong atau gak
            if ($value != "") {
                # code... masuk ke database
                LinkExcel::create([
                    'user_id' => $user->id,
                    'excel_id' => $request->excel_id,
                    'account' => $request->akun,
                    'link' => $value,
                    'sosialmedia' => $request->sosmed
                ]);
            }
        }
        return back();
        
    }
    public function destroy(Request $request)
    {
        # code...
        LinkExcel::destroy($request->id);
        return back();

    }

    public function export(Request $request, $id){

        // $spreadsheet = new Spreadsheet();
        // $activeWorksheet = $spreadsheet->getActiveSheet();
        // $activeWorksheet->setCellValue('A1', 'Hello World !');

        $path = public_path('hehe')."\Master.xlsx";
        $reader = new ReaderXlsx();
        $spreadsheet = $reader->load($path);

        $YoutubeWorksheet = $spreadsheet->getSheetByName('YOUTUBE');
        $dataYoutube = LinkExcel::select('account','link')->where('excel_id',$id)->where('sosialmedia','Youtube')->get();
        // foreach ($dataYoutube as $key => $value) {
        //     # code...
        //     $row = 4+i;
        //     $YoutubeWorksheet->setCellValue("D{$row}", );
        // }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="myfile.xlsx"');
        header('Cache-Control: max-age=0');

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }


    function download(Request $request) {
        $filenamed = $request->filename;
        $pathToFile = public_path('download')."/".$filenamed;
        return response()->download($pathToFile); 
    }
}
