<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Excel;
class ExcelController extends Controller
{
    //
    public function index(){
        return Inertia::render('Excel', ['taskdata' => Excel::latest()->limit(10)->get()]);
        
    }
    public function store(Request $request)
    {
        # code...
        Excel::create([
            'name' => $request->task_input,
        ]);
        return back();
    }
    public function destroy(Request $request)
    {
        # code...
        Excel::destroy($request->id);
        return 'sukses';
    }
}
