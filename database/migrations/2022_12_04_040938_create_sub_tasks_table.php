<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_tasks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('task_id');
            $table->string('name');
            $table->string('deadline')->nullable();
            $table->integer('facebook')->default(0);
            $table->integer('instagram')->default(0);
            $table->integer('twitter')->default(0);
            $table->integer('tiktok')->default(0);
            $table->integer('youtube')->default(0);
            $table->integer('snackvideo')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_tasks');
    }
};
