<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_excels', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('excel_id');
            $table->string('account');
            $table->string('link');
            $table->string('sosialmedia');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_excels');
    }
};
